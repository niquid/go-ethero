package crypto

import (
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"

	"golang.org/x/crypto/sha3"
)

type Address []byte

func (addr Address) HexAddress() string {
	return fmt.Sprintf("0x%s", hex.EncodeToString(addr))
}

// Keccak256 calculates and returns the Keccak256 hash of the input data.
func Keccak256(data ...[]byte) []byte {
	d := sha3.NewLegacyKeccak256()
	for _, b := range data {
		d.Write(b)
	}
	return d.Sum(nil)
}

func PubkeyToAddress(p *ecdsa.PublicKey) Address {
	pubBytes := FromECDSAPub(p)
	return Address(Keccak256(pubBytes[1:])[12:])
}
