package crypto

import (
	"errors"
)

var (
	errInvalidPubkey = errors.New("invalid secp256k1 public key")
)
