package secp256k1

import (
	"crypto/ecdsa"
	"errors"
	"fmt"
	"math/big"
)

type Signature struct {
	R *big.Int
	S *big.Int
	V *byte
}

func (curve *secp256k1Curve) Verify(pub *ecdsa.PublicKey, digestHash []byte, sig Signature) bool {
	if pub == nil || pub.Curve != curve {
		return false
	}
	if curve.HalfN.Cmp(sig.S) == -1 {
		return false
	}
	return ecdsa.Verify(pub, digestHash, sig.R, sig.S)
}

// recoverKeyFromSignature recovers a public key from the signature "sig" on the
// given message hash. Based on the algorithm found in section 4.1.6 of
// SEC 1 Ver 2.0, page 47-48 (53 and 54 in the pdf). This performs the details
// in the inner loop in Step 1. The counter provided is actually the j parameter
// of the loop * 2 - on the first iteration of j we do the R case, else the -R
// case in step 1.6. This counter is used in the bitcoin compressed signature
// format and thus we match bitcoind's behaviour here.
func (curve *secp256k1Curve) RecoverKeyFromSignature(
	hash []byte,
	sig Signature,
	j int,
	doChecks bool,
) (*ecdsa.PublicKey, error) {
	// 1.1 x = (n * i) + r
	Rx := new(big.Int).Mul(curve.N, new(big.Int).SetInt64(int64(j/2)))
	Rx.Add(Rx, sig.R)
	if Rx.Cmp(curve.P) != -1 {
		return nil, errors.New("calculated Rx is larger than curve P")
	}

	// convert 02<Rx> to point R. (step 1.2 and 1.3). If we are on an odd
	// iteration then 1.6 will be done with -R, so we calculate the other
	// term when uncompressing the point.
	Ry, err := curve.DecompressY(Rx, uint(j%2))
	if err != nil {
		return nil, err
	}

	// 1.4 Check n*R is point at infinity
	if doChecks {
		nRx, nRy := curve.ScalarMult(Rx, Ry, curve.N.Bytes())
		if nRx.Sign() != 0 || nRy.Sign() != 0 {
			return nil, errors.New("n*R does not equal the point at infinity")
		}
	}

	// 1.5 calculate e from message using the same algorithm as ecdsa
	// signature calculation.
	if len(hash) != curve.ByteSize {
		return nil, fmt.Errorf("hash is not %d bytes long", curve.ByteSize)
	}
	e := new(big.Int).SetBytes(hash)

	// Step 1.6.1:
	// We calculate the two terms sR and eG separately multiplied by the
	// inverse of r (from the signature). We then add them to calculate
	// Q = r^-1(sR + -eG)
	invr := new(big.Int).ModInverse(sig.R, curve.N)

	// sR
	invrS := new(big.Int).Mul(sig.S, invr)
	invrS.Mod(invrS, curve.N)
	sRx, sRy := curve.ScalarMult(Rx, Ry, invrS.Bytes())

	// -eG
	e.Neg(e)
	e.Mod(e, curve.N)
	e.Mul(e, invr)
	e.Mod(e, curve.N)
	minuseGx, minuseGy := curve.ScalarBaseMult(e.Bytes())

	// TODO: this would be faster if we did a mult and add in one
	// step to prevent the jacobian conversion back and forth.
	Qx, Qy := curve.Add(sRx, sRy, minuseGx, minuseGy)

	return &ecdsa.PublicKey{
		Curve: curve,
		X:     Qx,
		Y:     Qy,
	}, nil
}

// SignatureFromBytes transforms byte slice to a Signature{R, S, V} struct
func SignatureFromBytes(sig []byte) Signature {
	var v *byte
	if len(sig) == 65 {
		v = &sig[64]
	} else if len(sig) != 64 {
		return Signature{}
	}

	return Signature{
		R: new(big.Int).SetBytes(sig[0:32]),
		S: new(big.Int).SetBytes(sig[32:64]),
		V: v,
	}
}
