package secp256k1

import (
	"crypto/ecdsa"
	"crypto/rand"
	"encoding/hex"
	"math/big"
	"testing"
)

var privKey, _ = newPrivFromHex("18E14A7B6A307F426A94F8114701E7C8E774E7F9A47E2C2035DB29A206321725")
var msg, _ = hex.DecodeString("000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f")

func newPrivFromHex(s string) (ecdsa.PrivateKey, error) {
	k, err := hex.DecodeString(s)
	if err != nil {
		return ecdsa.PrivateKey{}, err
	}

	x, y := SECP256K1().ScalarBaseMult(k)

	return ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{
			Curve: SECP256K1(),
			X:     x,
			Y:     y,
		},
		D: new(big.Int).SetBytes(k),
	}, nil
}

func BenchmarkSign(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _, err := ecdsa.Sign(rand.Reader, &privKey, msg)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkVerify(b *testing.B) {
	r, s, err := ecdsa.Sign(rand.Reader, &privKey, msg)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if !ecdsa.Verify(&privKey.PublicKey, msg, r, s) {
			b.Fatal("failed to verify signature")
		}
	}
}
