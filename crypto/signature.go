package crypto

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"errors"
	"fmt"
	"math/big"

	"go.niquid.tech/ethero/crypto/secp256k1"
)

var errInvalidSignatureV = errors.New("invalid signature recovery V")

// FromECDSAPub marshals the public key in uncompressed form
func FromECDSAPub(pub *ecdsa.PublicKey) []byte {
	if pub == nil || pub.X == nil || pub.Y == nil {
		return nil
	}
	return elliptic.Marshal(secp256k1.SECP256K1(), pub.X, pub.Y)
}

// GenerateKey creates a random private key
func GenerateKey() (*ecdsa.PrivateKey, error) {
	return ecdsa.GenerateKey(secp256k1.SECP256K1(), rand.Reader)
}

// VerifySignature checks that the given public key created signature over digest.
// The public key should be in compressed (33 bytes) or uncompressed (65 bytes) format.
// The signature should have the 64 byte [R || S] format.
func VerifySignature(pubkey, digestHash, signature []byte) bool {
	pub, err := ReadPubkey(pubkey)
	if err != nil {
		return false
	}
	if len(signature) != 64 {
		return false
	}
	curve := secp256k1.SECP256K1()
	sig := secp256k1.SignatureFromBytes(signature)
	return curve.Verify(pub, digestHash, sig)
}

// EcdsaRecoverPubkey returns the uncompressed public key that created the given signature.
func EcdsaRecoverPubkey(hash, signature []byte) (*ecdsa.PublicKey, error) {
	curve := secp256k1.SECP256K1()
	sig := secp256k1.SignatureFromBytes(signature)
	if sig.V == nil {
		return nil, errInvalidSignatureV
	}
	v := int(*sig.V - 27)
	if v > 1 {
		return nil, errInvalidSignatureV
	}
	pubkey, err := curve.RecoverKeyFromSignature(hash, sig, v, true)
	if err != nil {
		return nil, err
	}
	return pubkey, nil
}

// RecoverAddress extracts Ethereum address from a signature
func RecoverAddress(hash, signature []byte) (string, error) {
	p, err := EcdsaRecoverPubkey(hash, signature)
	if err != nil {
		return "", fmt.Errorf("failed ecrecover: %w", err)
	}
	return PubkeyToAddress(p).HexAddress(), nil
}

// ReadPubkey parses a public key in the 33-byte compressed format.
func ReadPubkey(pubkey []byte) (*ecdsa.PublicKey, error) {
	var x, y *big.Int
	var err error

	curve := secp256k1.SECP256K1()
	if len(pubkey) == 65 && pubkey[0] == 0x04 {
		x, y = elliptic.Unmarshal(curve, pubkey)
		if x == nil {
			err = errInvalidPubkey
		}
	} else if len(pubkey) == 33 && (pubkey[0] == 0x02 || pubkey[0] == 0x03) {
		ybit := uint(pubkey[0] & 1)
		x = new(big.Int).SetBytes(pubkey[1:])
		y, err = curve.DecompressY(x, ybit)
	} else {
		return nil, fmt.Errorf("invalid public key format")
	}
	if err != nil {
		return nil, err
	}
	return &ecdsa.PublicKey{
		Curve: curve,
		X:     x,
		Y:     y,
	}, nil
}

// CompressPubkey encodes a public key to the 33-byte compressed format.
func CompressPubkey(pubkey *ecdsa.PublicKey) []byte {
	out := make([]byte, 33)
	format := byte(0x02)
	if isOdd(pubkey.Y) {
		format |= 0x01
	}
	out[0] = format
	copy(out[33-len(pubkey.X.Bytes()):], pubkey.X.Bytes())
	return out
}

func isOdd(i *big.Int) bool {
	return new(big.Int).Mod(i, big.NewInt(2)).Uint64() == 1
}
