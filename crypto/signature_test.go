package crypto

import (
	"bytes"
	"crypto/ecdsa"
	"encoding/hex"
	"math/big"
	"reflect"
	"testing"

	"go.niquid.tech/ethero/crypto/secp256k1"
)

var (
	testmsg, _     = hex.DecodeString("ce0677bb30baa8cf067c88db9811f4333d131bf8bcf12fe7065d211dce971008")
	testsig, _     = hex.DecodeString("90f27b8b488db00b00606796d2987f6a5f59ae62ea05effe84fef5b8b0e549984a691139ad57a3f0b906637673aa2f63d1f55cb1a69199d4009eea23ceaddc931c")
	testpubkey, _  = hex.DecodeString("04e32df42865e97135acfb65f3bae71bdc86f4d49150ad6a440b6f15878109880a0a2b2667f7e725ceea70c673093bf67663e0312623c8e091b13cf2c0f11ef652")
	testpubkeyc, _ = hex.DecodeString("02e32df42865e97135acfb65f3bae71bdc86f4d49150ad6a440b6f15878109880a")
	test2sig, _    = hex.DecodeString("a010d4cd27b1dfb7640fe064d009e54e67eedcfb481238e92972fa67912eadb278845636bb07b70be49dc2277408fc17cddbb576213fa1715f9a424d0d7109aa1c")
	test2msg, _    = hex.DecodeString("ca8abc0c7b620ca0a2a8a058fbbc2e2001669c93e8afb38cff036e853be46cd2")
	test2pubkey, _ = hex.DecodeString("04033f6eea6784ae3038f83c1ed12c5f01e67bc7342a2e58229f14ce13e03b1590fa4651bcbd85913292a74d4f4a66a1f9c0dc525d17e79868334dd18b7ac441f2")
)

func copyBytes(src []byte) []byte {
	dst := make([]byte, len(src))
	copy(dst, src)
	return dst
}

func TestEcdsaRecoverPubkey(t *testing.T) {
	t.Run("pubkey1", func(t *testing.T) {
		pubkey, err := EcdsaRecoverPubkey(testmsg, testsig)
		if err != nil {
			t.Errorf("EcdsaRecoverPubKey failed: %w", err)
		}
		have := FromECDSAPub(pubkey)
		want := testpubkey
		if !bytes.Equal(have, want) {
			t.Errorf("pubkey mismatch:\n want: %x\n have: %x", want, have)
		}
	})
	t.Run("pubkey2", func(t *testing.T) {
		pubkey, err := EcdsaRecoverPubkey(test2msg, test2sig)
		if err != nil {
			t.Errorf("EcdsaRecoverPubKey failed: %w", err)
		}
		have := FromECDSAPub(pubkey)
		want := test2pubkey
		if !bytes.Equal(have, want) {
			t.Errorf("pubkey mismatch:\n want: %x\n have: %x", want, have)
		}
	})
}

func TestRecoverAddress(t *testing.T) {
	have, err := RecoverAddress(test2msg, test2sig)
	if err != nil {
		t.Errorf("failed to recover address")
	}
	want := "0x3d035a1e9b552002ccce42c51f4be14db49258b2"
	if have != want {
		t.Errorf("pubkey mismatch:\n want: %x\n have: %x", want, have)
	}
}

func TestVerifySignature(t *testing.T) {
	sig := testsig[:len(testsig)-1] // remove recovery id
	if !VerifySignature(testpubkey, testmsg, sig) {
		t.Errorf("can't verify signature with uncompressed key")
	}
	if !VerifySignature(testpubkeyc, testmsg, sig) {
		t.Errorf("can't verify signature with compressed key")
	}

	if VerifySignature(nil, testmsg, sig) {
		t.Errorf("signature valid with no key")
	}
	if VerifySignature(testpubkey, nil, sig) {
		t.Errorf("signature valid with no message")
	}
	if VerifySignature(testpubkey, testmsg, nil) {
		t.Errorf("nil signature valid")
	}
	if VerifySignature(testpubkey, testmsg, append(copyBytes(sig), 1, 2, 3)) {
		t.Errorf("signature valid with extra bytes at the end")
	}
	if VerifySignature(testpubkey, testmsg, sig[:len(sig)-2]) {
		t.Errorf("signature valid even though it's incomplete")
	}
	wrongkey := copyBytes(testpubkey)
	wrongkey[10]++
	if VerifySignature(wrongkey, testmsg, sig) {
		t.Errorf("signature valid with with wrong public key")
	}
}

// This test checks that VerifySignature rejects malleable signatures with s > N/2.
func TestVerifySignatureMalleable(t *testing.T) {
	sig, _ := hex.DecodeString("638a54215d80a6713c8d523a6adc4e6e73652d859103a36b700851cb0e61b66b8ebfc1a610c57d732ec6e0a8f06a9a7a28df5051ece514702ff9cdff0b11f454")
	key, _ := hex.DecodeString("03ca634cae0d49acb401d8a4c6b6fe8c55b70d115bf400769cc1400f3258cd3138")
	msg, _ := hex.DecodeString("d301ce462d3e639518f482c7f03821fec1e602018630ce621e1e7851c12343a6")
	if VerifySignature(key, msg, sig) {
		t.Error("VerifySignature returned true for malleable signature")
	}
}

func TestDecompressPubkey(t *testing.T) {
	key, err := ReadPubkey(testpubkeyc)
	if err != nil {
		t.Fatal(err)
	}
	if uncompressed := FromECDSAPub(key); !bytes.Equal(uncompressed, testpubkey) {
		t.Errorf("wrong public key result: got %x, want %x", uncompressed, testpubkey)
	}
	if _, err := ReadPubkey(nil); err == nil {
		t.Errorf("no error for nil pubkey")
	}
	if _, err := ReadPubkey(testpubkeyc[:5]); err == nil {
		t.Errorf("no error for incomplete pubkey")
	}
	if _, err := ReadPubkey(append(copyBytes(testpubkeyc), 1, 2, 3)); err == nil {
		t.Errorf("no error for pubkey with extra bytes at the end")
	}
}

func TestCompressPubkey(t *testing.T) {
	x, _ := (&big.Int{}).SetString("e32df42865e97135acfb65f3bae71bdc86f4d49150ad6a440b6f15878109880a", 16)
	y, _ := (&big.Int{}).SetString("0a2b2667f7e725ceea70c673093bf67663e0312623c8e091b13cf2c0f11ef652", 16)
	key := &ecdsa.PublicKey{
		Curve: secp256k1.SECP256K1(),
		X:     x,
		Y:     y,
	}
	compressed := CompressPubkey(key)
	if !bytes.Equal(compressed, testpubkeyc) {
		t.Errorf("wrong public key result: got %x, want %x", compressed, testpubkeyc)
	}
}

func TestPubkeyRandom(t *testing.T) {
	const runs = 200

	for i := 0; i < runs; i++ {
		key, err := GenerateKey()
		if err != nil {
			t.Fatalf("iteration %d: %v", i, err)
		}
		pubkey, err := ReadPubkey(CompressPubkey(&key.PublicKey))
		if err != nil {
			t.Fatalf("iteration %d: %v", i, err)
		}
		if !reflect.DeepEqual(key.PublicKey, *pubkey) {
			t.Fatalf("iteration %d: keys not equal", i)
		}
	}
}

func BenchmarkEcdsaRecoverPubKeySignature(b *testing.B) {
	for i := 0; i < b.N; i++ {
		if _, err := EcdsaRecoverPubkey(testmsg, testsig); err != nil {
			b.Fatal("EcdsaRecoverPubkey error", err)
		}
	}
}

func BenchmarkVerifySignature(b *testing.B) {
	sig := testsig[:len(testsig)-1] // remove recovery id
	for i := 0; i < b.N; i++ {
		if !VerifySignature(testpubkey, testmsg, sig) {
			b.Fatal("verify error")
		}
	}
}

func BenchmarkDecompressPubkey(b *testing.B) {
	for i := 0; i < b.N; i++ {
		if _, err := ReadPubkey(testpubkeyc); err != nil {
			b.Fatal(err)
		}
	}
}
